﻿using System;

namespace apithingy {
    public class SecurityUtil {
        
        /**
         * <summary>Creates a new unique session id</summary>
         * <remarks>This might not be perfectly secure since <code>Guid.NewGuid()</code> is probably pseudo-random</remarks>
         */
        public static string CreateSessionId() {
            string token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            return token;
        }
    }
}