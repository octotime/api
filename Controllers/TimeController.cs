﻿using Microsoft.AspNetCore.Mvc;

namespace apithingy.Controllers {
    
    [ApiController]
    [Route("[controller]")]
    public class TimeController : ControllerBase {

        [HttpGet]
        public IActionResult Get([FromQuery(Name = "apiKey")] string key) {
            return Ok(RestConsumer.GetUser(key));
        }
    }
}