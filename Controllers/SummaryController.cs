﻿using System;
using System.Globalization;
using Microsoft.AspNetCore.Mvc;

namespace apithingy.Controllers {
    
    [ApiController]
    [Route("[controller]")]
    public class SummaryController : ControllerBase {
        
        [HttpGet]
        public IActionResult Get([FromQuery(Name = "apiKey")] string key, [FromQuery(Name = "start")] string start, [FromQuery] string end) {
            if (start == null) {
                return BadRequest("Missing parameter: start");
            }

            DateTime startParsed;
            if (!DateTime.TryParseExact(start, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startParsed)) {
                return BadRequest("Could not parse date: start");
            }

            if (startParsed.CompareTo(DateTime.Today) == 1) {
                return BadRequest("Start date cant be in the future.");
            }

            DateTime endParsed;
            if (end == null) {
                end = startParsed.ToString("yyyy-MM-dd");
            } else if (!DateTime.TryParseExact(end, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out endParsed)) {
                return BadRequest("Could not parse date: end");
            }
            else if (endParsed.CompareTo(startParsed) >= 1) {
                return BadRequest("End date cant be after start date.");
            }
            return Ok(RestConsumer.GetSummary(key, start, end));
        }
    }
}