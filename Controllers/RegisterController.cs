﻿using Microsoft.AspNetCore.Mvc;

namespace apithingy.Controllers {
    
    [ApiController]
    [Route("[controller]")]
    public class RegisterController : ControllerBase {
        [HttpPost]
        public IActionResult Register(RegisterBody body) {
            string sessionId = SecurityUtil.CreateSessionId();
            // register user here
            Response.Cookies.Append("sessionId", sessionId);
            return Ok("");
        }
    }

    public class RegisterBody {
        public string Email { get; set; }
        public string Password { get; set; }
        public string WakaApiKey { get; set; }
    }
}