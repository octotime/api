﻿using Microsoft.AspNetCore.Mvc;

namespace apithingy.Controllers {
    
    [ApiController]
    [Route("[controller]")]
    public class StatsController : ControllerBase {
        [HttpGet]
        public IActionResult Get([FromQuery(Name = "apiKey")] string key) {
            var result = RestConsumer.GetStats(key);
            if (result != null) {
                return Ok(result);
            }
            return Ok(RestConsumer.GetStats(key));
        }
    }
}