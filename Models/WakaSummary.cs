﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace apithingy.Models {
    public class WakaSummary {

        [JsonProperty("range")]
        public Range Range { get; set; }
        
        [JsonProperty("grand_total")]
        public TotalTime GrandTotal { get; set; }
        
        [JsonProperty("editors")]
        public List<WakaProp> Ides { get; set; }

        [JsonProperty("languages")]
        public List<WakaProp> Languages { get; set; }
    }

    public class WakaProp {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("percent")]
        public double Percent { get; set; }
        [JsonProperty("total_seconds")]
        public double TotalSeconds { get; set; }
    }

    public class Range {
        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("start")]
        public string Start { get; set; }

        [JsonProperty("end")]
        public string End { get; set; }
    }
    
    public class TotalTime {
        [JsonProperty("total_seconds")]
        public double TotalSeconds { get; set; }
    }
}