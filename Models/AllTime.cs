﻿using Newtonsoft.Json;

namespace apithingy.Models {
    public class AllTime {
        [JsonProperty("total_seconds")]
        public double Seconds { get; set; }
        [JsonProperty("is_up_to_date")]
        public bool IsUpToDate { get; set; }
    }
}