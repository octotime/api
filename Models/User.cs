namespace apithingy.Models {
    public class User {
        public string UID { get; set; }
        public string Email { get; set; }
        public string LastHeartBeat { get; set; }
        public string ColorScheme { get; set; }
        public string Photo { get; set; }
        public bool PhotoPublic { get; set; } = false;
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
    }
}