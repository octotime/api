﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace apithingy.Models {
    public class WakaStats {

        [JsonProperty("total_seconds")]
        public float TotalSeconds { get; set; }
        
        [JsonProperty("daily_average")]
        public float DailyAverage { get; set; }

        [JsonProperty("holidays")]
        public int Holidays { get; set; }

        [JsonProperty("projects")]
        public List<Stat> Projects { get; set; }
        
        [JsonProperty("editors")]
        public List<Stat> Editors { get; set; }
        
        [JsonProperty("languages")]
        public List<Stat> Languages { get; set; }

        [JsonProperty("dependencies")]
        public List<Stat> Dependencies { get; set; }
    }
}