﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace apithingy.Models {
    public class WakaUser {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("photo")]
        public string Photo { get; set; }

        [JsonProperty("last_heartbeat_at")]
        public string LastHeartbeatAt { get; set; }

        [JsonProperty("last_plugin_name")]
        public string LastHeartbeatPlugin { get; set; }
    }
}