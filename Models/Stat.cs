﻿using Newtonsoft.Json;

namespace apithingy.Models {
    public class Stat {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("total_seconds")]
        public float TotalSeconds { get; set; }

        [JsonProperty("percent")]
        public float Percent { get; set; }
    }
}