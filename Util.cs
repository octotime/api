﻿using System;
using System.Globalization;

namespace apithingy {
    public class Util {
        // This presumes that weeks start with Monday.
        // Week 1 is the 1st week of the year with a Thursday in it.
        public static int GetIso8601WeekOfYear(DateTime time) {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday) {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek,
                DayOfWeek.Monday);
        }

        public enum vars {
            DB_IP,
            DB_PORT,
            DB_USER,
            DB_PASSWORD,
            DB_NAME,
            DB_USE_SSL,
        }

        /**
         * <summary>Get a specific environment variable</summary>
         * <param name="var">The name of the variable you want to retrieve</param>
         */
        public static string getEnvVar(vars var) {
            return System.Environment.GetEnvironmentVariable(var.ToString(), EnvironmentVariableTarget.Machine);
        }
    }
}