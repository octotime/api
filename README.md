# Octotime API Server

This project not only provides a Wrapper for the [Wakatime API](https://wakatime.com/), but it also saves your stats daily allowing you to look at your historical data even using the free plan.

## Setup
It's easiest to use the provided Docker container, but you should also be able to either compile the container yourself or even compile the project without Docker.

### Example docker-compose.yml

```yaml
version: '3.9'

services:
    api-server:
      image: registry.gitlab.com/octotime/api:<VERSION>
      restart: unless-stopped
      container_name: octotime
      ports:
        - "50:80"
      environment:
        - DB_IP=127.0.0.1
        - DB_PORT=3306
        - DB_USER=octotime
        - DB_PASSWORD=example
        - DB_NAME=octotime
        - DB_USE_SSL=true
```

### Environment variables

 |  Variable    |       Explanation                         |
 | ------------ | ----------------------------------------- |
 | DB_IP        | The IP of the database server             |
 | DB_PORT      | The port of the database server           |
 | DB_USER      | The database user                         |
 | DB_PASSWORD  | The password for the database             |
 | DB_NAME      | The database name you want to user        |
 | DB_USE_SSL   | Whether the database server supports SSL  |