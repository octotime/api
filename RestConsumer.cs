using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apithingy.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace apithingy {
    public interface RestService {
        Task<WakaStats> GetStats(string apiKey);
    }

    public class RestConsumer {
        public static WakaUser GetUser(string ApiKey) {
            var client = new RestClient("https://wakatime.com/api/v1/users/current?api_key=" + ApiKey);
            var response = client.Execute(new RestRequest());
            if (!response.IsSuccessful) return null;
            var o = JObject.Parse(response.Content);
            if (!o.ContainsKey("data")) return null;
            var data = o.SelectToken("$.data");
            if (data != null) {
                return data.ToObject<WakaUser>();
            }
            return null;
        }

        public static AllTime GetAllTime(string apiKey) {
            var client = new RestClient("https://wakatime.com/api/v1/users/current/all_time_since_today?api_key=" + apiKey);
            var response = client.Execute(new RestRequest());
            if (!response.IsSuccessful) return null;
            var o = JObject.Parse(response.Content);
            if (!o.ContainsKey("data")) return null;
            var data = o.SelectToken("$.data");
            if (data != null) {
                return data.ToObject<AllTime>();
            }
            return null;
        }
        
        public static WakaStats GetStats(string apiKey) {
            var client = new RestClient("https://wakatime.com/api/v1/users/current/stats/last_year?api_key=" + apiKey);
            var response =  client.Execute(new RestRequest());
            if (!response.IsSuccessful) return null;
            var o = JObject.Parse(response.Content);
            if (!o.ContainsKey("data")) return null;
            var data = o.SelectToken("$.data");
            if (data != null) {
                if (data["is_up_to_date"] != null && (bool)data.SelectToken("is_up_to_date") == false) {
                    Task.Delay(1000);
                    return GetStats(apiKey);
                }
                return data.ToObject<WakaStats>();
            }
            return null;
        }

        public static List<WakaSummary> GetSummary(string apiKey, string start, string end) {
            var client = new RestClient("https://wakatime.com/api/v1/users/current/summaries?start=" + start + "&end=" + end + "&api_key=" + apiKey);
            var response =  client.Execute(new RestRequest());
            if (!response.IsSuccessful) return null;
            var responseJson = JObject.Parse(response.Content);
            if (!responseJson.ContainsKey("data")) return null;
            var resultObjects = AllChildren(responseJson)
                .First(child => child.Type == JTokenType.Array && child.Path.Contains("data"))
                .Children<JObject>();
            
            var wakaSummaries = new List<WakaSummary>();
            foreach (JObject result in resultObjects) {
                wakaSummaries.Add(result.ToObject<WakaSummary>());
            }
            return wakaSummaries;
        }

        private static IEnumerable<JToken> AllChildren(JToken json) {
            foreach (var child in json.Children()) {
                yield return child;
                foreach (var childChild in AllChildren(child)) {
                    yield return childChild;
                }
            }
        }
    }
}